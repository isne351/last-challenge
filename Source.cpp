#include <iostream>
using namespace std;

void swap(int arr[], int index1, int index2) {
	int hold = arr[index1];
	arr[index1] = arr[index2];
	arr[index2] = hold;
}

void quicksort(int arr[], int l, int r) { 
	int i = l, j = r;
	int pivot = arr[(l + r) / 2]; 
	while (i <= j) { 
		while (arr[i] < pivot) {
			i++;
		}
		while (arr[j] > pivot) {
			j--;
		}
		if (i <= j) {
			swap(arr, i, j); //swap array
			i++;
			j--;
		}
	}
	if (l < j) {
		quicksort(arr, l, j);
	}
	if (r < r) {
		quicksort(arr, i, r);
	}
}

void quicksort(int arr[],int size) {
	if (size > 2) { 
		quicksort(arr, 0, size - 1); // recursive funciton 
	}
}

void mergeshort(int arr[], int l, int r, int chunk) {

	int n1 = chunk - l + 1;
	int n2 = r - chunk;

	int *L = new int[chunk]; // tmp 
	int *R = new int[chunk]; // tmp

	
	for (int i = 0; i < n1; i++) {
		L[i] = arr[l + i];
	}

	for (int j = 0; j < n2; j++) {
		R[j] = arr[chunk + 1 + j];
	}


	int i = 0;
	int j = 0; 
	int k = l; 

	while (i < n1 && j < n2){ // if some one false break

		if (L[i] <= R[j]){
			arr[k] = L[i];
			i++;
		}else{
			arr[k] = R[j];
			j++;
		}

		k++;
	}


	while (i < n1){ // copy remain left array to main array
		arr[k] = L[i];
		i++;
		k++;
	}

	while (j < n2){ // copy remain right array to main array

		arr[k] = R[j];
		j++;
		k++;
	}


}

void mergeshort(int arr[], int l, int r) { // split function devide array by chunk size 

	if (l < r) { // break loop

		int chunk = l + (r - l) / 2; // chunk size

		mergeshort(arr, l, chunk);
		mergeshort(arr, chunk + 1, r);

		mergeshort(arr, l, r,chunk);
	}

}

void mergeshort(int arr[], int size) { // interface function receicve array/size from user

	mergeshort(arr,0,size-1);// array , first index , last index

}

int main() {

	int merg[11] = { 1,2,5,4,3,7,3,5,1,2,3};
	int quick[11] = { 1,2,5,4,3,7,3,5,1,2,3 };

	cout << "Source = ";

	for (int i = 0; i < 11; i++) {
		cout << merg[i] << " ";
	}

	cout << endl;

	cout << "Merg   = ";

	mergeshort(merg, 11);

	for (int i = 0; i < 11; i++) {
		cout << merg[i] << " ";
	}

	cout << endl;

	cout << "Quick  = ";

	quicksort(quick,11);

	for (int i = 0; i < 11; i++) {
		cout << quick[i] << " ";
	}

	cout << endl;

	system("pause");

	return true;
}